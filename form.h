#ifndef FORM_H
#define FORM_H
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QWidget>
#include <QProcess>
#include <QString>
#include <QUrl>
#include <QDebug>
#include <QTimer>
#include "json.h"
#include <QMap>
typedef QMap<QString, QString> Test;
using namespace QtJson;
namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form(QWidget *parent = 0);
    ~Form();
    
private:
    Ui::Form *ui;
    Test tests;
    public slots:    void get_id(QString);
    public slots:    void add_answer();
    QByteArray GET(QUrl);
};

#endif // FORM_H
