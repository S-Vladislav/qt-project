#-------------------------------------------------
#
# Project created by QtCreator 2014-12-22T22:56:12
#
#-------------------------------------------------

QT       += core gui network

TARGET = untitled1
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    json.cpp \
    form.cpp \
    table_form.cpp \
    question.cpp

HEADERS  += widget.h \
    json.h \
    form.h \
    table_form.h \
    question.h

FORMS    += widget.ui \
    form.ui \
    table_form.ui \
    question.ui
