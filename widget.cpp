#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{

   ui->setupUi(this);

    connect(ui->addTest,SIGNAL(clicked()),this,SLOT(add_test()));

    connect(ui->OpenButton,SIGNAL(clicked()),this,SLOT(open_window()));

   connect(ui->openTable,SIGNAL(clicked()),this,SLOT(open_table()));

}



Widget::~Widget()
{
    delete ui;
}

void Widget::get_test()
{

    QUrl current("http://middle.dev/test/registration/");

    current.addQueryItem("1","2");

    QByteArray answer = GET(current);

   // qDebug() << answer;

    QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

    for (int i = 0; i < Test.size(); i++) {

        QVariantMap current = Test[i].toMap();
        QString tmp = current.value("name").toString() + " " + current.value("theme").toString();
        tests[tmp] = current.value("id").toString();

    }

 //   for (Test::iterator itr = tests.begin(); itr!=tests.end();itr++)
       // ui->comboBox->addItem(itr.key());

}

void Widget::add_test()
{
    QUrl current("http://middle.dev/test/addTest/");

    current.addQueryItem("name",ui->nameTest->text());
    current.addQueryItem("theme",ui->themeTest->text());
    QByteArray answer = GET(current);

    QString response = parse(answer).toMap().value("message").toString();

    if (response=="success") {

        ui->nameTest->setText("");
        ui->themeTest->setText("");
        QMessageBox::information(0,"Information", "Test add!");
    } else {
        ui->nameTest->setText("");
        ui->themeTest->setText("");
        QMessageBox::warning(0,"Warning", "Test not add!");
    }
}

void Widget::open_window()
{
    form.show();
}

void Widget::open_table()
{
    table.show();
}



QByteArray Widget::GET(QUrl r)
{

    QNetworkAccessManager* manager = new QNetworkAccessManager(this);

    QNetworkReply* reply = manager->get(QNetworkRequest(r));

    QEventLoop wait;

    connect(manager,SIGNAL(finished(QNetworkReply*)),&wait,SLOT(quit()));

    QTimer::singleShot(10000,&wait,SLOT(quit()));

    wait.exec();

    QByteArray answer  = reply->readAll();

    reply->deleteLater();

    return answer;


}

