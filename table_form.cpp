#include "table_form.h"
#include "ui_table_form.h"

Table_form::Table_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Table_form)
{
    ui->setupUi(this);

    model.setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
   model.setHorizontalHeaderItem(1, new QStandardItem(QString("Название")));
     model.setHorizontalHeaderItem(2, new QStandardItem(QString("Тема")));

   model.index(1,1,model.index(0,0));
   //model.setHorizontalHeaderLabels(horizontalHeader);
   QUrl current("http://middle.dev/test/getTest");

   QByteArray answer = GET(current);

   QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

       for (int j = 0; j<5; j++) {

           for (int j = 0; j < Test.size(); j++) {

               QVariantMap current = Test[j].toMap();
               item00 = new QStandardItem(QString(current.value("id").toString()));
              model.setItem(j,0, item00);
                item00 = new QStandardItem(QString(current.value("name").toString()));
               model.setItem(j,1, item00);
               item00 = new QStandardItem(QString(current.value("theme").toString()));
               model.setItem(j,2, item00);

           }



       }
       //create contextmenu
       copyAction = new QAction(tr("&Copy"), this);
       copyAction->setStatusTip(tr("Copy Table Location"));
       copyAction->setShortcut(tr("Ctrl+C"));
       connect(copyAction, SIGNAL(triggered()), this, SLOT(aCopy()));

       pasteAction = new QAction(tr("&Paste"), this);
       pasteAction->setStatusTip(tr("Paste"));
       pasteAction->setShortcut(tr("Ctrl+V"));
       connect(pasteAction, SIGNAL(triggered()), this, SLOT(aPaste()));

       ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
       connect(ui->tableView, SIGNAL(customContextMenuRequested( const QPoint& )),
               this, SLOT(tablev_customContextMenu( const QPoint& )));

       //connect horizontal header QTableView with contextmenu
//       horizontalHeader = ui->tableView->horizontalHeader();
//       horizontalHeader->setContextMenuPolicy(Qt::CustomContextMenu);     //set contextmenu
//       connect(horizontalHeader, SIGNAL(customContextMenuRequested( const QPoint& )),
//               this, SLOT(tablev_customContextMenu( const QPoint& )));

       //connect vertical header QTableView with contextmenu
//       verticalHeader = ui->tableView->verticalHeader();
//       verticalHeader->setContextMenuPolicy(Qt::CustomContextMenu);     //set contextmenu
//       connect(verticalHeader, SIGNAL(customContextMenuRequested( const QPoint& )),
//               this, SLOT(tablev_customContextMenu( const QPoint& )));
//       connect(verticalHeader, SIGNAL(customContextMenuRequested(QPoint)),
//               this, SLOT(aCopy(QAction*)));

        connect(ui->tableView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(onTableClicked(const QModelIndex &)));
        ui->tableView->horizontalHeader()->setResizeMode( 0, QHeaderView::Stretch );
        ui->tableView->setModel(&model);
//     ui->tableView->setColumnHidden(1, true);
        ui->tableView->resizeRowsToContents();
        ui->tableView->resizeColumnsToContents();
        connect(ui->delete_test,SIGNAL(clicked()),this,SLOT(delete_test()));
        connect(ui->update_test,SIGNAL(clicked()),this,SLOT(update_test()));

        connect(ui->openTableTest,SIGNAL(clicked()),this,SLOT(open_table_question()));



}
void Table_form::resizeEvent(QResizeEvent *event) {
        ui->tableView->setColumnWidth(0, this->width()/3);
        ui->tableView->setColumnWidth(1, this->width()/3);
        ui->tableView->setColumnWidth(2, this->width()/3);

        QWidget::resizeEvent(event);
}
void Table_form::onTableClicked(const QModelIndex &index)
{
    if (index.isValid()) {
        QString cellText = index.data().toString();
        QUrl current("http://middle.dev/test/getTestById/");
        current.addQueryItem("id_test",cellText);
        QByteArray answer = GET(current);

        QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();
        QVariantMap test_one = Test[0].toMap();
        ui->id_test->setText(test_one.value("id").toString());
        ui->name_test->setText(test_one.value("name").toString());
        ui->theme_test->setText(test_one.value("theme").toString());
        ui->count_question->setText(test_one.value("Total_question").toString());
        int time;
        time =  test_one.value("Total_question").toInt()*2;
        QString count_time = QString::number(time);
        ui->time_question->setText(count_time + " минут");
    }
}

void Table_form::delete_test()
{
    QString id;
    id = ui->id_test->text();
    QUrl current("http://middle.dev/test/deleteTest/");
    current.addQueryItem("id_test",id);
    QByteArray answer = GET(current);
    qDebug() << answer << current;
    ui->id_test->setText("");
    ui->name_test->setText("");
    ui->theme_test->setText("");
    ui->count_question->setText("0");
    ui->time_question->setText("0");
    update_table();
}



void Table_form::update_test()
{
    QString id,name,theme;
    id = ui->id_test->text();
    name = ui->name_test->text();
    theme = ui->theme_test->text();
    QUrl current("http://middle.dev/test/updateTest/");
    current.addQueryItem("id_test",id);
    current.addQueryItem("name_test",name);
    current.addQueryItem("theme_test",theme);
    QByteArray answer = GET(current);
    update_table();
}

void Table_form::open_table_question()
{
        question.show();
}
Table_form::~Table_form()
{
    delete ui;
}

bool Table_form::boolGetItemTableView(QTableView *table)
{
    QItemSelectionModel * model = table->selectionModel();

    if(model)
        return (true);
    else
        return (false);

}

void Table_form::tablev_customContextMenu(const QPoint &)
{
    if(boolGetItemTableView(ui->tableView))
    {
        QMenu menu(this);
        menu.addAction(copyAction);
        menu.addAction(pasteAction);
        menu.exec(QCursor::pos());
    }
}

QByteArray Table_form::GET(QUrl r)
{
    QNetworkAccessManager* manager = new QNetworkAccessManager(this);

    QNetworkReply* reply = manager->get(QNetworkRequest(r));

    QEventLoop wait;

    connect(manager,SIGNAL(finished(QNetworkReply*)),&wait,SLOT(quit()));

    QTimer::singleShot(10000,&wait,SLOT(quit()));

    wait.exec();

    QByteArray answer  = reply->readAll();

    reply->deleteLater();

    return answer;

}

void Table_form::onRowChanged(QModelIndex index)
{
    int row = index.row();
    QString s = QString::number(row);

    QUrl current("http://middle.dev/test/getTestById");
    current.addQueryItem("id_test",s);
    QByteArray answer = GET(current);

    QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

    qDebug() << Test;
}
void Table_form::aCopy()
{

}

void Table_form::aPaste()
{

}
void Table_form::update_table()
{
   model.setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
   model.setHorizontalHeaderItem(1, new QStandardItem(QString("Название")));
   model.setHorizontalHeaderItem(2, new QStandardItem(QString("Тема")));

   model.index(1,1,model.index(0,0));
   QUrl current("http://middle.dev/test/getTest");

   QByteArray answer = GET(current);

   QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

       for (int j = 0; j<5; j++) {

           for (int j = 0; j < Test.size(); j++) {

               QVariantMap current = Test[j].toMap();
               item00 = new QStandardItem(QString(current.value("id").toString()));
              model.setItem(j,0, item00);
                item00 = new QStandardItem(QString(current.value("name").toString()));
               model.setItem(j,1, item00);
               item00 = new QStandardItem(QString(current.value("theme").toString()));
               model.setItem(j,2, item00);

           }

       }
       //create contextmenu
       copyAction = new QAction(tr("&Copy"), this);
       copyAction->setStatusTip(tr("Copy Table Location"));
       copyAction->setShortcut(tr("Ctrl+C"));
       connect(copyAction, SIGNAL(triggered()), this, SLOT(aCopy()));

       pasteAction = new QAction(tr("&Paste"), this);
       pasteAction->setStatusTip(tr("Paste"));
       pasteAction->setShortcut(tr("Ctrl+V"));
       connect(pasteAction, SIGNAL(triggered()), this, SLOT(aPaste()));
       ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
       connect(ui->tableView, SIGNAL(customContextMenuRequested( const QPoint& )),this, SLOT(tablev_customContextMenu( const QPoint& )));
       connect(ui->tableView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(onTableClicked(const QModelIndex &)));
       ui->tableView->horizontalHeader()->setResizeMode( 0, QHeaderView::Stretch );
       ui->tableView->setModel(&model);
       ui->tableView->resizeRowsToContents();
       ui->tableView->resizeColumnsToContents();
       ui->tableView->setColumnWidth(0, this->width()/3);
       ui->tableView->setColumnWidth(1, this->width()/3);
       ui->tableView->setColumnWidth(2, this->width()/3);

}
