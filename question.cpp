#include "question.h"
#include "ui_question.h"

Question::Question(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Question)
{
    ui->setupUi(this);

   model_question.setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
   model_question.setHorizontalHeaderItem(1, new QStandardItem(QString("ID теста")));
   model_question.setHorizontalHeaderItem(2, new QStandardItem(QString("Текст вопроса")));
   model_question.setHorizontalHeaderItem(3, new QStandardItem(QString("Ответ 1")));
   model_question.setHorizontalHeaderItem(4, new QStandardItem(QString("Ответ 2")));
   model_question.setHorizontalHeaderItem(5, new QStandardItem(QString("Ответ 3")));
   model_question.setHorizontalHeaderItem(6, new QStandardItem(QString("Правильный ответ")));
   model_question.index(1,1,model_question.index(0,0));

   QUrl current("http://middle.dev/test/getAllQuestion");

   QByteArray answer = GET(current);

   QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

       for (int j = 0; j<6; j++) {

           for (int j = 0; j < Test.size(); j++) {

               QVariantMap current = Test[j].toMap();
               item00 = new QStandardItem(QString(current.value("id").toString()));
              model_question.setItem(j,0, item00);
                item00 = new QStandardItem(QString(current.value("id_test").toString()));
               model_question.setItem(j,1, item00);
               item00 = new QStandardItem(QString(current.value("question_text").toString()));
               model_question.setItem(j,2, item00);
               item00 = new QStandardItem(QString(current.value("answer_text_one").toString()));
               model_question.setItem(j,3, item00);
               item00 = new QStandardItem(QString(current.value("answer_text_two").toString()));
               model_question.setItem(j,4, item00);
               item00 = new QStandardItem(QString(current.value("answer_text_three").toString()));
               model_question.setItem(j,5, item00);
               item00 = new QStandardItem(QString(current.value("answer_true").toString()));
               model_question.setItem(j,6, item00);
           }



       }

        connect(ui->tableView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(onTableClicked(const QModelIndex &)));
        ui->tableView->horizontalHeader()->setResizeMode( 0, QHeaderView::Stretch );
        ui->tableView->setModel(&model_question);
        ui->tableView->resizeRowsToContents();
        ui->tableView->resizeColumnsToContents();
        connect(ui->deleteQuestion,SIGNAL(clicked()),this,SLOT(delete_test()));
        connect(ui->UpdateButton,SIGNAL(clicked()),this,SLOT(update_test()));


}

Question::~Question()
{
    delete ui;
}
void Question::resizeEvent(QResizeEvent *event) {
    ui->tableView->setColumnWidth(0, this->width()/3);
    ui->tableView->setColumnWidth(1, this->width()/3);
    ui->tableView->setColumnWidth(2, this->width()/3);
    ui->tableView->setColumnWidth(3, this->width()/3);
    ui->tableView->setColumnWidth(4, this->width()/3);
    ui->tableView->setColumnWidth(5, this->width()/3);
    ui->tableView->setColumnWidth(6, this->width()/3);

        QWidget::resizeEvent(event);
}
void Question::onTableClicked(const QModelIndex &index)
{
    if (index.isValid()) {
        QString cellText = index.data().toString();
        QUrl current("http://middle.dev/test/getQuestionById/");
        current.addQueryItem("id_question",cellText);
        QByteArray answer = GET(current);

        QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();
        QVariantMap question_one = Test[0].toMap();
        ui->id_test->setText(question_one.value("id_test").toString());
        ui->id_question->setText(question_one.value("id").toString());
        ui->question_text->setText(question_one.value("question_text").toString());
        ui->answer_text_one->setText(question_one.value("answer_text_one").toString());
        ui->answer_text_two->setText(question_one.value("answer_text_two").toString());
        ui->answer_text_three->setText(question_one.value("answer_text_three").toString());
        ui->answer_true->setText(question_one.value("answer_true").toString());
    }

}
void Question::delete_test()
{
    QString id;
    id = ui->id_question->text();
    QUrl current("http://middle.dev/test/deleteQuestion/");
    current.addQueryItem("id_question",id);
    QByteArray answer = GET(current);
    ui->id_test->setText("");
    ui->id_question->setText("");
    ui->question_text->setText("");
    ui->answer_text_one->setText("");
    ui->answer_text_two->setText("");
    ui->answer_text_three->setText("");
    ui->answer_true->setText("");

    update_table();
}

void Question::update_table()
{


    model_question.setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    model_question.setHorizontalHeaderItem(1, new QStandardItem(QString("ID теста")));
    model_question.setHorizontalHeaderItem(2, new QStandardItem(QString("Текст вопроса")));
    model_question.setHorizontalHeaderItem(3, new QStandardItem(QString("Ответ 1")));
    model_question.setHorizontalHeaderItem(4, new QStandardItem(QString("Ответ 2")));
    model_question.setHorizontalHeaderItem(5, new QStandardItem(QString("Ответ 3")));
    model_question.setHorizontalHeaderItem(6, new QStandardItem(QString("Правильный ответ")));
    model_question.index(1,1,model_question.index(0,0));

    QUrl current("http://middle.dev/test/getAllQuestion");

    QByteArray answer = GET(current);

    QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

        for (int j = 0; j<6; j++) {

            for (int j = 0; j < Test.size(); j++) {

                QVariantMap current = Test[j].toMap();
                item00 = new QStandardItem(QString(current.value("id").toString()));
               model_question.setItem(j,0, item00);
                 item00 = new QStandardItem(QString(current.value("id_test").toString()));
                model_question.setItem(j,1, item00);
                item00 = new QStandardItem(QString(current.value("question_text").toString()));
                model_question.setItem(j,2, item00);
                item00 = new QStandardItem(QString(current.value("answer_text_one").toString()));
                model_question.setItem(j,3, item00);
                item00 = new QStandardItem(QString(current.value("answer_text_two").toString()));
                model_question.setItem(j,4, item00);
                item00 = new QStandardItem(QString(current.value("answer_text_three").toString()));
                model_question.setItem(j,5, item00);
                item00 = new QStandardItem(QString(current.value("answer_true").toString()));
                model_question.setItem(j,6, item00);
            }



        }

         connect(ui->tableView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(onTableClicked(const QModelIndex &)));
         ui->tableView->horizontalHeader()->setResizeMode( 0, QHeaderView::Stretch );
         ui->tableView->setModel(&model_question);
         ui->tableView->resizeRowsToContents();
         ui->tableView->resizeColumnsToContents();
         connect(ui->deleteQuestion,SIGNAL(clicked()),this,SLOT(delete_test()));

         ui->tableView->setColumnWidth(0, this->width()/3);
         ui->tableView->setColumnWidth(1, this->width()/3);
         ui->tableView->setColumnWidth(2, this->width()/3);
         ui->tableView->setColumnWidth(3, this->width()/3);
         ui->tableView->setColumnWidth(4, this->width()/3);
         ui->tableView->setColumnWidth(5, this->width()/3);
         ui->tableView->setColumnWidth(6, this->width()/3);
}



void Question::update_test()
{
    QString id_test,id_question,question_text , answer_text_one,answer_text_two,answer_text_three,answer_true;

    id_test = ui->id_test->text();
    id_question = ui->id_question->text();
    question_text = ui->question_text->toPlainText();
    answer_text_one = ui->answer_text_one->toPlainText();
    answer_text_two = ui->answer_text_two->toPlainText();
    answer_text_three = ui->answer_text_three->toPlainText();
    answer_true = ui->answer_true->toPlainText();

    QUrl current("http://middle.dev/test/updateQuestion/");
    current.addQueryItem("id_test",id_test);
    current.addQueryItem("id_question",id_question);
    current.addQueryItem("question_text",question_text);
    current.addQueryItem("answer_text_one",answer_text_one);
    current.addQueryItem("answer_text_two",answer_text_two);
    current.addQueryItem("answer_text_three",answer_text_three);
    current.addQueryItem("answer_true",answer_true);
    QByteArray answer = GET(current);
    update_table();
}
QByteArray Question::GET(QUrl r)
{

    QNetworkAccessManager* manager = new QNetworkAccessManager(this);

    QNetworkReply* reply = manager->get(QNetworkRequest(r));

    QEventLoop wait;

    connect(manager,SIGNAL(finished(QNetworkReply*)),&wait,SLOT(quit()));

    QTimer::singleShot(10000,&wait,SLOT(quit()));

    wait.exec();

    QByteArray answer  = reply->readAll();

    reply->deleteLater();

    return answer;


}
