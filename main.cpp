#include <QtGui/QApplication>
#include "widget.h"
#include <QTextCodec>
int main(int argc, char *argv[])
{
    QTextCodec *utfcode  = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(utfcode);
    QTextCodec::setCodecForCStrings(utfcode);
    QApplication a(argc, argv);
    Widget w;
    w.show();
    
    return a.exec();
}
