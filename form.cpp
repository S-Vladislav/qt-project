#include "form.h"
#include "ui_form.h"
#include <QMessageBox>
Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    QUrl current("http://middle.dev/test/getTest/");

    QByteArray answer = GET(current);

    QVariantList Test = parse(answer).toMap().value("response").toMap().value("items").toList();

    for (int i = 0; i < Test.size(); i++) {

        QVariantMap current = Test[i].toMap();
        QString tmp = current.value("name").toString();
        tests[tmp] = current.value("id").toString();

    }

    for (Test::iterator itr = tests.begin(); itr!=tests.end();itr++) {
           QString num=itr.value();
           int n = num.toInt();
           ui->comboBox->setCurrentIndex(n);
           ui->comboBox->addItem(itr.key());
    }

     connect(ui->comboBox,SIGNAL(currentIndexChanged(QString)),this,SLOT(get_id(QString)));

     connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(add_answer()));

}

Form::~Form()
{
    delete ui;
}

void Form::get_id(QString name)
{
    QUrl current("http://middle.dev/test/getTestByName/");
    current.addQueryItem("name",name);
    QByteArray answer = GET(current);
    QString response = parse(answer).toMap().value("id").toString();
    ui->idtest->setText(response);
}

void Form::add_answer()
{
    QUrl current("http://middle.dev/test/addQuestion/");
    current.addQueryItem("id_test",ui->idtest->text());

    /*

      1 Question

    */

    current.addQueryItem("question_1",ui->question_1->text());
    current.addQueryItem("answer_1_1",ui->answer_1_1->text());
    current.addQueryItem("answer_2_1",ui->answer_2_1->text());
    current.addQueryItem("answer_3_1",ui->answer_3_1->text());
    current.addQueryItem("answer_true_1",ui->answer_true_1->text());

    /*

      2 Question

    */

    current.addQueryItem("question_2",ui->question_3->text());
    current.addQueryItem("answer_1_2",ui->answer_1_4->text());
    current.addQueryItem("answer_2_2",ui->answer_2_4->text());
    current.addQueryItem("answer_3_2",ui->answer_3_4->text());
    current.addQueryItem("answer_true_2",ui->answer_true_4->text());

    /*

      3 Question

    */

    current.addQueryItem("question_3",ui->question_4->text());
    current.addQueryItem("answer_1_3",ui->answer_1_5->text());
    current.addQueryItem("answer_2_3",ui->answer_2_5->text());
    current.addQueryItem("answer_3_3",ui->answer_3_5->text());
    current.addQueryItem("answer_true_3",ui->answer_true_5->text());

    /*

      4 Question

    */

    current.addQueryItem("question_4",ui->question_5->text());
    current.addQueryItem("answer_1_4",ui->answer_1_6->text());
    current.addQueryItem("answer_2_4",ui->answer_2_6->text());
    current.addQueryItem("answer_3_4",ui->answer_3_6->text());
    current.addQueryItem("answer_true_4",ui->answer_true_6->text());

    /*

      5 Question

    */

    current.addQueryItem("question_5",ui->question_6->text());
    current.addQueryItem("answer_1_5",ui->answer_1_7->text());
    current.addQueryItem("answer_2_5",ui->answer_2_7->text());
    current.addQueryItem("answer_3_5",ui->answer_3_7->text());
    current.addQueryItem("answer_true_5",ui->answer_true_7->text());


    /*

      6 Question

    */

    current.addQueryItem("question_6",ui->question_8->text());
    current.addQueryItem("answer_1_6",ui->answer_1_8->text());
    current.addQueryItem("answer_2_6",ui->answer_2_8->text());
    current.addQueryItem("answer_3_6",ui->answer_3_8->text());
    current.addQueryItem("answer_true_6",ui->answer_true_8->text());

    QByteArray answer = GET(current);

    QString response = parse(answer).toMap().value("message").toString();

    if (response=="success") {

//        ui->question_1->text("");
//        ui->answer_1_1->text("");
//        ui->answer_2_1->text("");
//        ui->answer_3_1->text("");
//        ui->answer_true_1->text("");

//        ui->question_3->text("");
//        ui->answer_1_4->text("");
//        ui->answer_2_4->text("");
//        ui->answer_3_4->text("");
//        ui->answer_true_4->text("");

//        ui->question_4->text("");
//        ui->answer_1_5->text("");
//        ui->answer_2_5->text("");
//        ui->answer_3_5->text("");
//        ui->answer_true_5->text("");

//        ui->question_5->text("");
//        ui->answer_1_6->text("");
//        ui->answer_2_6->text("");
//        ui->answer_3_6->text("");
//        ui->answer_true_6->text("");

//        ui->question_6->text("");
//        ui->answer_1_7->text("");
//        ui->answer_2_7->text("");
//        ui->answer_3_7->text("");
//        ui->answer_true_7->text("");

//        ui->question_7->text("");
//        ui->answer_1_8->text("");
//        ui->answer_2_8->text("");
//        ui->answer_3_8->text("");
//        ui->answer_true_8->text("");

        QMessageBox::information(0,"Information", "Test add!");
    } else {

//        ui->question_1->text("");
//        ui->answer_1_1->text("");
//        ui->answer_2_1->text("");
//        ui->answer_3_1->text("");
//        ui->answer_true_1->text("");

//        ui->question_3->text("");
//        ui->answer_1_4->text("");
//        ui->answer_2_4->text("");
//        ui->answer_3_4->text("");
//        ui->answer_true_4->text("");

//        ui->question_4->text("");
//        ui->answer_1_5->text("");
//        ui->answer_2_5->text("");
//        ui->answer_3_5->text("");
//        ui->answer_true_5->text("");

//        ui->question_5->text("");
//        ui->answer_1_6->text("");
//        ui->answer_2_6->text("");
//        ui->answer_3_6->text("");
//        ui->answer_true_6->text("");

//        ui->question_6->text("");
//        ui->answer_1_7->text("");
//        ui->answer_2_7->text("");
//        ui->answer_3_7->text("");
//        ui->answer_true_7->text("");

//        ui->question_8->text("");
//        ui->answer_1_8->text("");
//        ui->answer_2_8->text("");
//        ui->answer_3_8->text("");
//        ui->answer_true_8->text("");

        QMessageBox::warning(0,"Warning", "Test not add!");
    }
}


QByteArray Form::GET(QUrl r)
{

    QNetworkAccessManager* manager = new QNetworkAccessManager(this);

    QNetworkReply* reply = manager->get(QNetworkRequest(r));

    QEventLoop wait;

    connect(manager,SIGNAL(finished(QNetworkReply*)),&wait,SLOT(quit()));

    QTimer::singleShot(10000,&wait,SLOT(quit()));

    wait.exec();

    QByteArray answer  = reply->readAll();

    reply->deleteLater();

    return answer;


}
