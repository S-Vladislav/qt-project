#ifndef TABLE_FORM_H
#define TABLE_FORM_H

#include <QWidget>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QNetworkAccessManager>
#include <QModelIndex>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QProcess>
#include <QString>
#include <QUrl>
#include <QDebug>
#include <QTimer>
#include "json.h"
#include <QMap>
 #include <QTableView>
#include <QMenu>
#include <question.h>
typedef QMap<QString, QString> Test;
using namespace QtJson;
namespace Ui {
class Table_form;
}

class Table_form : public QWidget
{
    Q_OBJECT
    
public:
    explicit Table_form(QWidget *parent = 0);
    ~Table_form();
    
private:
    Ui::Table_form *ui;
    Test tests;
    Question question;
    QStandardItemModel model;

    QModelIndex modelIndex;
    QAction *copyAction;
    QAction *pasteAction;
    QStandardItem *item00;

//    QString horizontalHeader;
//    QString verticalHeader;
    bool boolGetItemTableView(QTableView *table);
    QByteArray GET(QUrl);
   // public slots:    void get_row(QString);
    private slots:   void onRowChanged(QModelIndex index);
    private slots:  void tablev_customContextMenu( const QPoint& );
    public slots: void resizeEvent(QResizeEvent *event);
    private slots: void aCopy();
    private slots:    void aPaste();
    public slots: void onTableClicked(const QModelIndex &index);
    public slots: void delete_test();
    public slots: void update_table();
    public slots: void update_test();
    public slots:    void open_table_question();
};

#endif // TABLE_FORM_H
