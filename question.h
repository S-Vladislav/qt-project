#ifndef QUESTION_H
#define QUESTION_H
#include <QWidget>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QNetworkAccessManager>
#include <QModelIndex>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QProcess>
#include <QString>
#include <QUrl>
#include <QDebug>
#include <QTimer>
#include "json.h"
#include <QMap>
#include <QTableView>
#include <QMenu>
typedef QMap<QString, QString> Test;
using namespace QtJson;

namespace Ui {
class Question;
}

class Question : public QWidget
{
    Q_OBJECT
    
public:
    explicit Question(QWidget *parent = 0);
    ~Question();
    
private:
    Ui::Question *ui;
    QByteArray GET(QUrl);
    QStandardItemModel model_question;
    QModelIndex modelIndex;
    QStandardItem *item00;
    public slots: void onTableClicked(const QModelIndex &index);
    public slots: void resizeEvent(QResizeEvent *event);
    public slots: void delete_test();
    public slots: void update_table();
    public slots: void update_test();
};

#endif // QUESTION_H
