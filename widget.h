#ifndef WIDGET_H
#define WIDGET_H
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QWidget>
#include <QProcess>
#include <QString>
#include <QUrl>
#include <QDebug>
#include <QTimer>
#include "json.h"
#include <QMap>
#include <form.h>
#include <table_form.h>

typedef QMap<QString, QString> Test;
using namespace QtJson;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    
private:
    Ui::Widget *ui;
    Form form;
    Test tests;


  Table_form table;
    public slots:    void get_test();
    public slots:    void add_test();
    public slots:    void open_window();
    public slots:    void open_table();

    QByteArray GET(QUrl);
};

#endif // WIDGET_H
